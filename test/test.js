var req = require('supertest');

var app = require('../src/app.js')

describe('GET /', () => {
    it('Displays "Alive"', (done) =>{
        req(app).get('/').expect('Alive',done);
    });
});